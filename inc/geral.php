<?
$nomeSite			= 'Loja Central Equipamentos';
$slogan				= 'Cote com diversas empresas para locação de equipamentos ';
// $url				= 'https://www.lojacentralequipamentos.com.br/';
// $url				= 'https://localhost/site-solucs-2019/lojacentralequipamentos.com.br/';

$dir = $_SERVER['SCRIPT_NAME'];
$dir = pathinfo($dir);
$host = $_SERVER['HTTP_HOST'];
$http = $_SERVER['REQUEST_SCHEME'];
if ($dir["dirname"] == "/") { $url = $http."://".$host."/"; }
else { $url = $http."://".$host.$dir["dirname"]."/";  }
	
$emailContato		= 'everton.lima@solucoesindustriais.com.br';
$rua				= 'Rua Pequetita, 179';
$bairro				= 'Vila Olimpia';
$cidade				= 'São Paulo';
$UF					= 'SP';
$cep				= 'CEP: 04552-060';
$latitude			= '-22.546052';
$longitude			= '-48.635514';
$senhaEmailEnvia	= '102030'; // colocar senha do e-mail mkt@dominiodocliente.com.br
$explode			= explode("/", $_SERVER['PHP_SELF']);
$urlPagina 			= end($explode);
$urlPagina	 		= str_replace('.php','',$urlPagina);
$urlPagina 			== "index"? $urlPagina= "" : "";
//reCaptcha do Google
$siteKey = '6Lfc7g8UAAAAAHlnefz4zF82BexhvMJxhzifPirv';
$secretKey = '6Lfc7g8UAAAAAKi8al32HjrmsdwoFoG7eujNOwBI';


//Breadcrumbs
$caminho 			= '
<div class="breadcrumb">
    <div class="wrapper">
        <div class="bread__row">
<nav aria-label="breadcrumb">
  <ol id="breadcrumb" class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
      <a href="' . $url . '" itemprop="item" title="Home">
        <span itemprop="name"><i class="fa fa-home" aria-hidden="true"></i> Home »  </span>
      </a>
      <meta itemprop="position" content="1" />
    </li>
   
    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
      <span itemprop="name">' . $h1 . '</span>
      <meta itemprop="position" content="2" />
    </li>
  </ol>
</nav>
</div>
</div>
</div>
';
$caminho2	= '
<div id="breadcrumb" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" >
	<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
		<a href="'.$url.'produtos" title="Produtos" class="category" itemprop="url"><span itemprop="title"> Produtos </span></a> »
		<div itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
			<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>
		</div>
	</div>
</div>
';
$caminhoBread 			= '
<div class="wrapper">
	<div id="breadcrumb" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" >
		<a rel="home" itemprop="url" href="'.$url.'" title="Home"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
		<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>
	</div>
	<h1>'.$h1.'</h1>
</div>
</div>
';
$caminhoBread2	= '
<div class="wrapper">
<div id="breadcrumb" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" >
	<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
		<a href="'.$url.'informacoes" title="Informações" class="category" itemprop="url"><span itemprop="title"> Informações </span></a> »
		<div itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
			<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>
		</div>
	</div>
</div>
<h1>'.$h1.'</h1>
</div>

';

$caminhoinformacoes	= '

<div class="breadcrumb">
    <div class="wrapper">
        <div class="bread__row">
<nav aria-label="breadcrumb">
  <ol id="breadcrumb" class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
      <a href="' . $url . '" itemprop="item" title="Home">
        <span itemprop="name"><i class="fa fa-home" aria-hidden="true"></i> Home »  </span>
      </a>
      <meta itemprop="position" content="1" />
    </li>
    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
      <a  href="' . $url . 'informacoes" itemprop="item" title="Informações">
        <span itemprop="name">Informações »  </span>
      </a>
      <meta itemprop="position" content="2" />
    </li>
   
    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
      <span itemprop="name">' . $h1 . '</span>
      <meta itemprop="position" content="3" />
    </li>
  </ol>
</nav>
</div>
</div>
</div>

';

$caminhocalibracao	= '
<div class="wrapper">
<div id="breadcrumb" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" >
	<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
		<a href="'.$url.'produtos" title="Produtos" class="category" itemprop="url"><span itemprop="title"> Produtos </span></a> »
		<div itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
			<a href="'.$url.'calibracao-categoria" title="Calibração" class="category" itemprop="url"><span itemprop="title"> Calibração </span></a> »
			<div itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
				<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>
			</div>
		</div>
	</div>
</div>
</div>
';
$caminhocalibracao_de_balancas	= '
<div class="wrapper">
<div id="breadcrumb" itemscope itemtype="http://data-vocabulary.org/Breadcrumb" >
	<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> »
	<div itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
		<a href="'.$url.'produtos" title="Produtos" class="category" itemprop="url"><span itemprop="title"> Produtos </span></a> »
		<div itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
			<a href="'.$url.'calibracao-de-balancas-categoria" title="Calibração de Balanças" class="category" itemprop="url"><span itemprop="title"> Calibração de Balanças </span></a> »
			<div itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
				<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>
			</div>
		</div>
	</div>
</div>
</div>
';


$caminholocacao_de_equipamentos_para_construcao_civil	= '
<div class="breadcrumb">
    <div class="wrapper">
        <div class="bread__row">
<nav aria-label="breadcrumb">
  <ol id="breadcrumb" class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
      <a href="' . $url . '" itemprop="item" title="Home">
        <span itemprop="name"><i class="fa fa-home" aria-hidden="true"></i> Home »  </span>
      </a>
      <meta itemprop="position" content="1" />
    </li>
    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
      <a  href="' . $url . 'produtos" itemprop="item" title="Produtos">
        <span itemprop="name">Produtos »  </span>
      </a>
      <meta itemprop="position" content="2" />
    </li>
    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
      <a  href="' . $url . 'locacao-de-equipamentos-para-construcao-civil-categoria" itemprop="item" title="Locação de Equipamentos para Construção Civil">
        <span itemprop="name">Locação de Equipamentos para Construção Civil »  </span>
      </a>
      <meta itemprop="position" content="3" />
    </li>
   
    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
      <span itemprop="name">' . $h1 . '</span>
      <meta itemprop="position" content="4" />
    </li>
  </ol>
</nav>
</div>
</div>
</div>
';
?>