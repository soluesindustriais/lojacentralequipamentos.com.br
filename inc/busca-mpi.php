<h2>produtos e serviços relacionados
</h2>
<ul>
	<?php define ('ROOT', realpath(dirname(__FILE__)).'/');
		$limit = 4;
		include('inc/vetKey.php');
		include('inc/classes/strFuncoes.class.php');
		$str = new strFuncoes();
		$vetPalavra = $str->RetiraPreposicao($h1);
		$vetKey_PR = $str->buscaVetorAprox($vetPalavra, $vetKey, $limit, $h1);
		foreach ($vetKey_PR as $keyVet => $valSearch)
		{
			$key = $vetKey[$valSearch['id']]["key"];
			if($key == $h1) {
				continue;
			}
			$urlImagem = $vetKey[$valSearch['id']]["urlImagem"];
			$caminho = $vetKey[$valSearch['id']]["url"];
			$urlKeySearch = str_replace("-", "+", $vetKey[$valSearch['id']]["url"]);
			$textKey = $vetKey[$valSearch['id']]['desc'];
			echo "
			<li class=\"row\" >
				<a href=\"".$caminho."\" title=\"".$key."\">
					<img src=\"imagens/mpi/thumbs/".$urlImagem."\" alt=\"".$key."\" title=\"".$key."\" />
				</a>
				<h3>
				<a href=\"".$caminho."\" title=\"".$key."\">".$key."</a>
					</h3>
					<p>".$textKey."</p>
			</li>";
		} ?>
</ul>


<!-- Shark Orcamento MPI -->
<?php if ($ter != 1): ?>
<app-cotacao-solucs
appConfig='{"btnOrcamento": "#btnOrcamento", "titulo": ".active-menu-aside", "industria": "solucoes-industriais"}'
></app-cotacao-solucs>  
<script>
	// pluginOrcamentoJs.init({
	// industria: 'solucoes-industriais',
	// btnOrcamento: '.btn-produto',
	// titulo: '#sharkOrcamento'
	// });
	// $('.btn-produto').on('click', function () {
	// 	var title = $('h1').text();
	// 	$('#sharkOrcamento').html(title.trim());
	// });
</script>
<?php endif; ?>

<div class="api-mobile">
    <div class="aside-mpi">
	    <div style="display:flex;flex-wrap:wrap;flex-direction:column;">
	        <button id="btnOrcamento" class="btn-produto"><i class="fa fa-envelope"></i>
	        Solicite um Orçamento
	        </button>
	    </div>
	    <nav>
	        <ul>
	        <!-- <?php include ('inc/vetkey.php');?> -->
	        </ul>
	    </nav>
    </div>
</div>
