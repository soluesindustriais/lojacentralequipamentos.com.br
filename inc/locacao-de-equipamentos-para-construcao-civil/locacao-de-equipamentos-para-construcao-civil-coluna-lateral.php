<aside>
    <h2><a href="<?= $url ?>locacao-de-equipamentos-para-construcao-civil-categoria" title="Produtos relacionados <?= $nomeSite ?>"> Locação de equipamentos para construção civil<br>Produtos relacionados</a></h2>
    <nav>
        <ul class="relacionados-scroll"> <? include('inc/locacao-de-equipamentos-para-construcao-civil/locacao-de-equipamentos-para-construcao-civil-sub-menu.php'); ?> </ul>
    </nav> <br>
</aside>
<aside>
    <h2><a href="<?= $url ?>" title="Outras Categorias">Outras Categorias </a></h2>
    <nav>
        <ul> <? include('inc/locacao-de-equipamentos-para-construcao-civil/locacao-de-equipamentos-para-construcao-civil-sub-menu-categoria.php'); ?> </ul>
    </nav> <br>
</aside>