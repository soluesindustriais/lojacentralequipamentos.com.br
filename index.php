<?
$h1         = 'Aluguel de Equipamento';
$title      = 'Aluguel de Equipamentos | Cotações gratuitas';
$desc       = 'Faça cotações para aluguel de equipamentos em apenas um lugar, aqui na Aluguel de Equipamentos';
$var        = 'Home';
include('inc/head.php');
include('inc/fancy.php');
?>

</head>
<body>
<? include('inc/topo.php'); ?>
<section class="cd-hero">
  <div class="title-main"> <h1><?=$h1?></h1> </div>
  <ul class="cd-hero-slider autoplay">
    <li class="selected">
      <div class="cd-full-width">
        <h2>Aluguel de betoneira preço sp</h2>
        <p>O  <strong> <em> aluguel de betoneira preço sp </em></strong> é super indicado para a preparação de concreto com muita eficiência e agilidade durante a execução de obras. O equipamento passa por um processo de manutenção preventivo, visando garantir seu perfeito funcionamento na obra, todas as máquinas são equipadas com sistema de segurança normatizado tais como proteção do pinhão e cremalheira e caixa de chave liga e desliga com botão de emergência.</p>
        <a href="<?=$url?>aluguel-de-betoneira-preco-sp" class="cd-btn">Saiba mais</a>
      </div>
    </li>
    <li>
      <div class="cd-full-width">
        <h2>Aluguel de Máquinas e Equipamentos</h2>
        <p>Os Aluguel de Máquinas e Equipamentos são ideais para empresas que necessitam de praticidade e baixo custo, entre em contato agora mesmo e solicite uma cotação com dezenas de empresas especializadas na locação de diversos tipode de máquinas e equipamentos</p>
        <a href="<?=$url?>aluguel-de-maquinas-e-equipamentos" class="cd-btn">Saiba mais</a>
      </div>
    </li>
    <li>
      <div class="cd-full-width">
        <h2>Aluguel de ferramentas</h2>
        <p>O aluguel de ferramentas precisa ser feito de forma ágil e eficiente, disponibilizando equipamentos de grande eficiência, para que os consumidores consigam realizar suas obras da melhor maneira possível. Muitos produtos são necessários na construção, como por exemplo betoneiras e marteletes.</p>
        <a href="<?=$url?>aluguel-de-ferramentas" class="cd-btn">Saiba mais</a>
      </div>
    </li>
  </ul>
  <div class="cd-slider-nav">
    <nav>
      <span class="cd-marker item-1"></span>
      <ul>
        <li class="selected"><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
        <li><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
        <li><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
      </ul>
    </nav>
  </div>
</section>
<main> 
  <section class="wrapper-main" data-anime="in">
    <div class="main-center">
      <div class=" quadro-2 ">
       <div class="child-main">
         <h2>Equipamento de Ferramantas para Construção Civil</h2>
         <div class="div-img">
           <p >Inúmeros equipamentos compõem as ferramentas de uma construção, cada um direcionado para uma atividade específica, como os alicates, amplamente utilizadas pelos profissionais no canteiro de obras, eles se dividem nas seguintes categorias: alicate universal, alicate de pressão, alicate de corte frontal, alicate de corte diagonal e alicate de bico meia cana.</p>
         </div>
    
      <div class="child-2-main">    
        <div class=" incomplete-box" data-anime="up">
           <ul data-anime="in">
             <li>
               <p>Cada obra possui necessidades específicas, que demandam a locação de equipamentos diferenciados para vencer os mais diversos desafios, e eles precisam estar em seu perfeito estado, evitando toda e qualquer falha de uso. Há, porém, alguns materiais que são indispensáveis em qualquer canteiro de obras, tais como:</p>
              <li><i class="fas fa-angle-right"></i>Alicates;</li>
             <li><i class="fas fa-angle-right"></i>Arcos de serra;</li>
             <li><i class="fas fa-angle-right"></i>Betoneiras;</li>
             <li><i class="fas fa-angle-right"></i>Chaves;</li>
             <li><i class="fas fa-angle-right"></i>Compressor de ar;</li>
             <li><i class="fas fa-angle-right"></i>Entre outros.</li>
           </ul>
           <a href="<?=$url?>locacao-de-equipamentos-para-construcao-civil-categoria" class="btn-4">Saiba mais</a>
         </div>
        <div class="child-p" data-anime="right">
         <p>Há muitas outras situações em que a Locação de compactador, por exemplo, pode ser mais econômico do que a compra de um equipamento novo.</p> 
         <img src="<?=$url?>imagens/mpi/locacao-de-compactador-01.jpg" alt="Locação de Compactador" title="Locação de Compactador">
         </div>
      </div>
       </div> 
      </div>
    </div>
      <div id="content-icons">
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <img src="<?=$url?>imagens/tools.png" alt="Ferramentas" class="fa fa-wrench" aria-hidden="true"></i>
            <div>
              <p>Grande variedade de Ferramentas</p>
            </div>
          </div>
        </div>
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <img src="<?=$url?>imagens/wrench.png" alt="Quadros" class="fa fa-cogs" aria-hidden="true"></i>
            <div>
              <p>Manuteção de Equipamentos</p>
            </div>
          </div>
        </div>
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <img src="<?=$url?>imagens/technical-support.png" alt="Suporte tecnico" class="fa fa-location-arrow" aria-hidden="true"></i>
            <div>
              <p>Equipamentos para locação</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="wrapper-img">
      <div class="txtcenter">
        <h2>Produtos <b>Relacionados</b></h2>
      </div>
      <div class="content-icons">
        <div class="produtos-relacionados-1">
          <figure>
            <a href="<?=$url?>aluguel-de-compressor">
              <div class="fig-img" data-anime="in">
                <h2>Aluguel de Compressor</h2>
              </div>
            </a>
          </figure>
        </div>
        <div class="produtos-relacionados-2">
          <figure class="figure2">
            <a href="<?=$url?>aluguel-de-betoneira-preco-sp">
              <div class="fig-img2" data-anime="in">
                <h2>ALuguel de Betoneiras</h2>
                
              </div>
            </a>
          </figure>
        </div>
        <div class="produtos-relacionados-3">
          <figure>
            <a href="<?=$url?>aluguel-de-martelete-sp">
              <div class="fig-img" data-anime="in">
                <h2>Locação de Martelete</h2>  
              </div>
            </a>
          </figure>
        </div>
      </div>
    </section>
    <section class="wrapper-destaque">
      <div class="destaque txtcenter">
        <h2>Galeria de <b>Produtos</b></h2>
        <div class="center-block txtcenter">
          <ul class="gallery">
            <li><a href="<?=$url?>imagens/img-home/botoneira.jpg" class="lightbox" title="foto-01">
              <img src="<?=$url?>imagens/img-home/thumbs/foto-01.jpg" title="foto-01" alt="foto-01">
            </a>
          </li>
            <li><a href="<?=$url?>imagens/img-home/aluguel-de-botoneira.jpg" class="lightbox"  title="foto-02">
            <img src="<?=$url?>imagens/img-home/thumbs/foto-02.jpg" alt="foto-02" title="foto-02">
          </a>
        </li>
            <li><a href="<?=$url?>imagens/img-home/acionador.jpg" class="lightbox" title="ffoto-03">
              <img src="<?=$url?>imagens/img-home/thumbs/foto-03.jpg" alt="foto-03" title="foto-03">
        </a>
      </li>
            <li><a href="<?=$url?>imagens/img-home/Alisadora-Dupla.jpg" class="lightbox" title="foto-04">
        <img src="<?=$url?>imagens/img-home/thumbs/foto-04.jpg" alt="foto-04" title="foto-04">
      </a>
    </li>
            <li><a href="<?=$url?>imagens/img-home/lixadeira.jpg" class="lightbox" title="foto-05">
      <img src="<?=$url?>imagens/img-home/thumbs/foto-05.jpg" alt="foto-05"  title="foto-05">
    </a>
  </li>
            <li><a href="<?=$url?>imagens/img-home/aluguel-de-lixadeira.jpg" class="lightbox" title="foto-06">
    <img src="<?=$url?>imagens/img-home/thumbs/foto-06.jpg" alt="foto-06" title="foto-06">
  </a>
</li>
            <li><a href="<?=$url?>imagens/img-home/aluguel-de-martelete.jpg" class="lightbox" title="foto-07">
  <img src="<?=$url?>imagens/img-home/thumbs/foto-07.jpg" alt="foto-07" title="foto-07">
</a>
</li>
            <li><a href="<?=$url?>imagens/img-home/martelete.jpg" class="lightbox" title="foto-08">
<img src="<?=$url?>imagens/img-home/thumbs/foto-08.jpg" alt="foto-08" title="foto-08">
</a>
</li>
            <li><a href="<?=$url?>imagens/img-home/locacao-compactador-sapo.jpg" class="lightbox" title="foto-09">
<img src="<?=$url?>imagens/img-home/thumbs/foto-09.jpg" alt="foto-09" title="foto-09">
</a>
</li>
            <li><a href="<?=$url?>imagens/img-home/compactador.jpg" class="lightbox" title="foto-10">
<img src="<?=$url?>imagens/img-home/thumbs/foto-10.jpg" alt="foto-10" title="foto-10">
</a>
</li>
</ul>
</div>
</div>
</section>
</main>
<? include('inc/footer.php'); ?>
<link rel="stylesheet" href="<?=$url?>nivo/nivo-slider.css" media="screen">
<script  src="<?=$url?>nivo/jquery.nivo.slider.js"></script>
<script >
$(window).load(function() {
$('#slider').nivoSlider();
});
</script>
<script src="<?=$url?>hero/js/modernizr.js"></script>
<script src="<?=$url?>hero/js/main.js"></script>

</body>
</html>


