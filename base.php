<? $h1 = "ADICIONAR"; $title  = "ADICIONAR"; $desc = "ADICIONAR"; $key  = "ADICIONAR,ADICIONAR,ADICIONAR"; include('inc/head.php'); include('inc/fancy.php'); ?>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main>
		<div class="content">
			<section>
				<?=$caminho?>
				<br class="clear" />
				<h1><?=$h1?>
				</h1>
				<article>
					<div class="img-mpi">
						<a href="imagens/mpi/thumbs/ADICIONAR-01.jpg" title="<?=$h1?>" class="lightbox">
							<img src="imagens/mpi/ADICIONAR-01.jpg" title="<?=$h1?>" alt="<?=$h1?>">
						</a>
						<a href="imagens/mpi/thumbs/ADICIONAR-02.jpg" title="<?=$h1?>" class="lightbox">
							<img src="imagens/mpi/ADICIONAR-02.jpg" title="<?=$h1?>" alt="<?=$h1?>">
						</a>
						<a href="imagens/mpi/thumbs/ADICIONAR-03.jpg" title="<?=$h1?>" class="lightbox">
							<img src="imagens/mpi/ADICIONAR-03.jpg" title="<?=$h1?>" alt="<?=$h1?>">
						</a>
					</div>
					<span class="aviso">Estas imagens foram obtidas de bancos de imagens públicas e disponível livremente na internet
					</span>
					<hr />
					<p>A
						<strong>bateria 60 amperes
						</strong> é o carro-chefe no mercado automotivo, já que elas são usadas para dar energia aos veículos. Dependendo dos itens que exigem força elétrica no veículo, como por exemplo um caminhão, maior será a amperagem. Os proprietários de automóveis, geralmente, desconhecem a função da amperagem de uma bateria. Sabem apenas que é necessário ter uma que suporte a “potência” do veículo, mas não compreendem informações básicas contidas no item.
					</p>
					<p>O preço de
						<strong>bateria 60 amperes
						</strong> tem um ótimo custo-benefício nos veículos em que este item se enquadra. De maneira simples, a baterias 60 amp gera energia elétrica para o funcionamento de um carro, por exemplo. Mas para que isso aconteça, existem outros itens dentro do automóvel que, em conjunto, dão a partida quando o condutor gira a chave na ignição. Esta é a mais utilizadas em veículos de passeio, pois consegue atender muito bem, sem sobrecarregar ou parar de funcionar.
					</p>
					<h2>Níveis de amperagem
					</h2>
					<ul>
						<li class="li-mpi">40 amperes
						</li>
						<li class="li-mpi">45 amperes
						</li>
						<li class="li-mpi">50 amperes
						</li>
						<li class="li-mpi">65 amperes
						</li>
						<li class="li-mpi">70 amperes
						</li>
						<li class="li-mpi">80 amperes
						</li>
						<li class="li-mpi">90 amperes
						</li>
					</ul>
					<h2>Especificações da
					<strong>bateria 60 amperes
					</strong> e outros produtos
					</h2>
					<p>As baterias 60 amp tem um processo de fabricação e especificações da ABNT NBR 15940/2016, INMETRO e conta com as licenças da Polícia Civil (Meio Ambiente), Corpo de Bombeiros, CETESB, IBAMA e Polícia Federal.
					</p>
					<p>Esta bateria, a bateria 60 ah, pode ser aplicada em veículos de passeio, utilitários, caminhões, carretas, tratores, ônibus, de marcas e modelos variados, para lado esquerdo ou direito. O importante é o usuário contar com uma inspeção preventiva de profissionais para selecionar o modelo ideal, evitando gastos e manutenções futuras ou até mesmo avarias.
					</p>
					<p>Existem também o carregador de bateria é um tipo de produto usado por pessoas físicas e empresas dos mais variados segmentos para que as baterias voltem a funcionar da melhor maneira possível, isso quando a bateria para de funcionar por falta de manutenções preventivas e corretivas.
					</p>
					<p>No mercado existem inúmeros modelos de carregador de bateria em várias potências e para diversas aplicações, auxiliando na economia, pois não será necessário a aquisição de novas baterias.
					</p>
					<p>Aproveite e solicite um orçamento agora mesmo da bateria 60 ah e outras.
					</p>
				</article>
				<? include('inc/coluna-mpi.php');?>
				<br class="clear">
				<? include('inc/busca-mpi.php');?>
				<? include('inc/form-mpi.php');?>
				<? include('inc/regioes.php');?>
			</section>
		</div>
	</main>
</div>
<? include('inc/footer.php');?>
</body>
</html>