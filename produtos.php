<?
$h1 = "Produtos";
$title = "Produtos";
$desc = "Encontre diversos fornecedores de grama sintética e quadras poliesportivas. Clique aqui para saber mais detalhes. Dúvidas, entre em contato conosco agora mesmo";
$var = "Produtos";
include('inc/head.php');
?>
</head>

<body>
	<? include('inc/topo.php'); ?>
	<div class="wrapper">
		<main>
			<div class="content">
				<div class="breadcrumb">
					<div class="wrapper">
						<div class="bread__row">
							<nav aria-label="breadcrumb">
								<ol id="breadcrumb" class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
									<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
										<a href="' . $url . '" itemprop="item" title="Home">
											<span itemprop="name"><i class="fa fa-home" aria-hidden="true"></i> Home » </span>
										</a>
										<meta itemprop="position" content="1" />
									</li>
									<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
										<span itemprop="name">Produtos</span>
										<meta itemprop="position" content="2" />
									</li>
								</ol>
							</nav>
						</div>
					</div>
				</div>


				<h1>Produtos</h1>
				<article class="full">
					<p>Encontre diversos fornecedores de grama sintética e quadras poliesportivas, cote agora mesmo!</p>
					<ul class="thumbnails-main">
						<li>
							<a rel="nofollow" href="<?= $url ?>locacao-de-equipamentos-para-construcao-civil-categoria" title="Transmissão">
								<img src="<? $url ?>imagens/locacao-de-equipamentos-para-construcao-civil/locacao-de-equipamentos-para-construcao-civil-1.jpg" alt="Locação de equipamentos para construção civil" title="Locação de equipamentos para construção civil" />
							</a>
							<h2>
								<a href="<?= $url ?>locacao-de-equipamentos-para-construcao-civil-categoria" title="Transmissão">
									Locação de equipamentos para contrução civil
								</a>
							</h2>
						</li>




					</ul>
				</article>
			</div>
		</main>
	</div>
	<? include('inc/footer.php'); ?>
</body>

</html>